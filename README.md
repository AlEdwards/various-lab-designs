# This repository contains designs and 3D printable mesh files for labware suitable for microbiology laboratory research

There are two folders containing open source licensed files: 
1. design files for editing using openscad CAD software (www.openscad.org) with file type .scad
2. 3-dimensional mesh files that can be used to 3D print parts

[](https://openscad.org/)


Summary overview of designs, files and stl file downloads to be found in this repository.
- Frame dip slide: a simple frame to create multi-agar dip-slides that can be dipped into 50mL tubes
- Multi-channel rectangle dish: A solid medium (e.g. agar) culture dish with multiple chambers to simplify replica plating on multiple agar types
- Lid of rectangle dish: Simple lid to prevent evaporation for multi-agar dish above
- Round pin replicator: Pin design for making a pin replicator can deposit bacterial samples from 96-well plate onto a round petri dish
- Holder for pin replicator: This holder is where the pins above fit
- Loop: This design is for 3D printed inoculation loops
- Row column 48-pin replicator: this is another pin replicator design to transfer bacterial samples in a grid onto square petri dishes


Link to download STL files from Mendely data
 	Frame dip slide	http://dx.doi.org/10.17632/scz5fnzmrx.1
 	Multi-channel rectangle dish	http://dx.doi.org/10.17632/kytj6c7372.1
 	Lid of rectangle dish	http://dx.doi.org/10.17632/cj8j75jvc5.1
 	Round pin replicator – pins	http://dx.doi.org/10.17632/zgc7js44cd.1
 	Holder for pin replicator	http://dx.doi.org/10.17632/s4r3nf6gbw.1
 	Loop	http://dx.doi.org/10.17632/drzy24kmjt.1
 	Row column 48-pin replicator	http://dx.doi.org/10.17632/2pzd37wpkc.1


