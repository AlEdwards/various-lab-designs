
$fn=100;
for (i=[0:5], p= [0:2]){
    translate ([10*p,i*10,1]) cylinder (r=2.5,h=10);
}


// x vertical blot
translate ([30,10,1]) cylinder (r=2.5,h=10);

translate ([30,20,1]) cylinder (r=2.5,h=10);

translate ([30,30,1]) cylinder (r=2.5,h=10);


translate ([30,40,1]) cylinder (r=2.5,h=10);

// y horizontal blot

translate ([-10,10,1]) cylinder (r=2.5,h=10);

translate ([-10,20,1]) cylinder (r=2.5,h=10);

translate ([-10,30,1]) cylinder (r=2.5,h=10);
translate ([-10,40,1]) cylinder (r=2.5,h=10);


//nut for pin row 
for (i=[0:5], p= [0:2]){
    translate ([10*p,10*i,0]) cylinder (r=3.5,h=2);
}

//nut for pin row outside
// x vertical blot
translate ([30,10,0]) cylinder (r=3.5,h=2);

translate ([30,20,0]) cylinder (r=3.5,h=2);

translate ([30,30,0]) cylinder (r=3.5,h=2);


translate ([30,40,0]) cylinder (r=3.5,h=2);

// y horizontal blot

translate ([-10,10,0]) cylinder (r=3.5,h=2);

translate ([-10,20,0]) cylinder (r=3.5,h=2);

translate ([-10,30,0]) cylinder (r=3.5,h=2);

translate ([-10,40,0]) cylinder (r=3.5,h=2);


