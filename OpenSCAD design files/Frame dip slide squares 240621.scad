$fn=100;

translate ([-12,-35,-8.6])
//bottom line
cube ([24,80,0.5]);

translate ([-12,-35,-8.6])
difference (){
//top of slide
translate ([0,0,0]) cube ([24,80,4]);


// cube for identification
    
translate ([16,2,0]) cube ([7, 35, 4]);
translate ([16,38,0]) cube ([7, 35, 4]);
    
 // second cube
 translate ([8.5,2,0]) cube ([7, 35, 4]);
translate ([8.5,38,0]) cube ([7, 35, 4]); 
    
 // third cube
 translate ([0.9,2,0]) cube ([7, 35, 4]);
translate ([0.9,38,0]) cube ([7, 35, 4]);   
    
}


//padle small
translate ([0,48,-1])
cube([24.3,8,15.3], center = true);
//padle big
difference(){
translate ([0,53,0])
cube ([28,10,19.5], center= true);

translate ([1.5,68,0])
rotate ([90,0,0])
linear_extrude (82)
difference(){
square (13.5);
circle (13.5);
}

translate ([-1.5,-14,0])
rotate ([-90,180,0])
linear_extrude (82)
difference(){
square (13.5);
circle (13.5);
}
}

