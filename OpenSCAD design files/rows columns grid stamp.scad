
col=8;
row=6;
wid=col*9+15;
leng=row*9+10;
stick=12;

e = ["0","1","2","3","4","5","6","7","8","9","10","11","12"];
f = ["0","a","b","c","d","e","f","g","h"];

mirror([1,0,0]) union(){

for (i = [1:col],j = [1:row]) translate([9*i,leng-9*j,0]) union() {
  translate([1.5,0,stick])  linear_extrude(1) text(e[i],size = 3.5,font="Liberation Sans:style=Bold");
  translate([0,1,stick])  linear_extrude(1) text(f[j],size = 3.5,font="Liberation Sans:style=Bold");  translate([2.5,2.5,0])    cylinder(d = 6,h=stick);
}
difference(){
cube([wid,leng,2]);
translate([5,5,-10]) cylinder(d=4,h=20,$fn=12);
translate([wid-5,5,-10]) cylinder(d=4,h=20,$fn=12);
translate([wid-5,leng-5,-10]) cylinder(d=4,h=20,$fn=12);
}

}