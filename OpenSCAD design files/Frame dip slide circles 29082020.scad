
translate ([-12.2,-35,-7.5])
//bottom line
cube ([24,80,0.5]);

translate ([-12.2,-35,-7.5])
difference (){
//top of slide
translate ([0,0,0]) cube ([24,80,4]);

//well
for (i=[0.7:10])
{
    
    translate ([4.5,7.2*i,3]) sphere (3.5);
    translate ([12,7.2*i,3])sphere (3.5);
    
// cube for identification
    
translate ([16,2,0]) cube ([7, 35, 4]);
translate ([16,38,0]) cube ([7, 35, 4]); 
}
}

//padle
translate ([0,48,0])
cube([24.5,8,15], center = true);

difference(){
translate ([0,53,0])
cube ([28,10,23], center= true);

translate ([1,68,0])
rotate ([90,0,0])
linear_extrude (82)
difference(){
square (14);
circle (14);
}

translate ([-1,-14,0])
rotate ([-90,180,0])
linear_extrude (82)
difference(){
square (14);
circle (14);
}
}

